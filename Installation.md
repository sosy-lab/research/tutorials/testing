# Windows

## Installation of CPAchecker

- Get the Java SDK, e.g. from [Oracle](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (jdk-8u161-windows-x64.exe for 64-bit systems, jdk-8u161-windows-i586.exe for 32-bit systems, ca. 200MB) and install
- Download the latest version of CPAchecker as [zip](https://cpachecker.sosy-lab.org/CPAchecker-1.7-windows.zip) file (ca. 46MB) from the [CPAchecker website](https://cpachecker.sosy-lab.org/download.php) and extract it

The contents of the extracted folder for CPAchecker should look like this:

![Contents of the CPAchecker folder](./images/cpafolder.png)
<!--<img src="./cpafolder.png" style="width:100px;"/>-->

## Is it working?

In order to check if everything works as expected, open the powershell (windows key + r, type in powershell, press enter), go into the CPAchecker directory and execute the following command:

    .\scripts\cpa.bat -predicateAnalysis .\doc\examples\example.c -setprop solver.solver=SMTInterpol -setprop cpa.predicate.encodeBitvectorAs=INTEGER -setprop cpa.predicate.encodeFloatAs=RATIONAL

The additional options with `-setprop` are necessary under Windows only, the default solver for CPAchecker is not yet supported.
The output should look like in the following picture and a new folder named output should appear in the CPAchecker directory:

![powershell commands and new folder in CPAchecker directory](./images/powershell.png)

The output folder contains a file named Report.html which can be viewed in the browser.

# Linux

## Installation of CPAchecker

- install a Java Runtime Environment (Ubuntu: `sudo apt-get install openjdk-8-jre`)

        sudo apt-get install openjdk-8-jre

- Download the latest version of CPAchecker as [tarball](https://cpachecker.sosy-lab.org/CPAchecker-1.7-unix.tar.bz2CPAchecker-1.7-windows.zip) file (ca. 83MB) from the [CPAchecker website](https://cpachecker.sosy-lab.org/download.php) and extract it

        curl -O https://cpachecker.sosy-lab.org/CPAchecker-1.7-unix.tar.bz2CPAchecker-1.7-windows.zip
        tar -xf CPAchecker-1.7-unix.tar.bz2

## Is it working?

In order to check if everything works as expected, go into the CPAchecker directory and try if this simple analysis will run successfully:

    ~ $ cd CPAchecker-1.7-unix/
    ~/CPAchecker-1.7-unix $ ./scripts/cpa.sh -predicateAnalysis doc/examples/example.c

The output should look like this:

    Running CPAchecker with default heap size (1200M). Specify a larger value with -heap if you have more RAM.
    Running CPAchecker with default stack size (1024k). Specify a larger value with -stack if needed.
    Using the following resource limits: CPU-time limit of 900s (ResourceLimitChecker.fromConfiguration, INFO)
    
    CPAchecker 1.7 (Java HotSpot(TM) 64-Bit Server VM 1.8.0_161) started (CPAchecker.run, INFO)
    
    MathSAT5 is available for research and evaluation purposes only. It can not be used in a commercial environment, particularly as part of a commercial product, without written permission. MathSAT5 is provided as is, without any warranty. Please write to mathsat@fbk.eu for additional questions regarding licensing MathSAT5 or obtaining more up-to-date versions. (PredicateCPA:JavaSMT:Mathsat5SolverContext.<init>, WARNING)

    Using predicate analysis with MathSAT5 version 5.5.0 (59014c8d54e1) (Dec  5 2017 11:43:02, gmp 6.1.0, gcc 4.8.5, 64-bit, reentrant) and JFactory 1.21. (PredicateCPA:PredicateCPA.<init>, INFO)

    MathSAT5 is available for research and evaluation purposes only. It can not be used in a commercial environment, particularly as part of a commercial product, without written permission. MathSAT5 is provided as is, without any warranty. Please write to mathsat@fbk.eu for additional questions regarding licensing MathSAT5 or obtaining more up-to-date versions. (ARGCPA:JavaSMT:Mathsat5SolverContext.<init>, WARNING)

    Using refinement for predicate analysis with PredicateAbstractionRefinementStrategy strategy. (PredicateCPA:PredicateCPARefiner.<init>, INFO)

    Starting analysis ... (CPAchecker.runAlgorithm, INFO)

    Stopping analysis ... (CPAchecker.runAlgorithm, INFO)

    Verification result: TRUE. No property violation found by chosen configuration.
    More details about the verification run can be found in the directory "./output".
    Graphical representation included in the file "./output/Report.html"

A new folder named output should appear in the CPAchecker directory. This folder contains a file named Report.html that can be viewed in the browser.

## Installation of KLEE

### Docker

KLEE is best used with a docker container (so they say).
How to install docker in Ubuntu is described [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04).
Installing docker on Ubuntu is not that easy unfortunately, because it is not in the regular repository *sigh*
Installation of KLEE using docker is described [here](http://klee.github.io/docker/).
It boils down to:

    docker pull klee/klee
    docker run --rm -ti --ulimit='stack=-1:-1' klee/klee

The container can be left with ctrl+d or exit, as you would exit any terminal.
If you want to make a folder on the host, e.g. the current working directory,  accessible to the klee container:

    docker run --rm -ti --ulimit='stack=-1:-1' -v $(pwd):/home/klee/dir klee/klee

This will make the pwd on the host available under /home/klee/dir in the container

### With binaries contained in TBF

KLEE binaries are contained in tbf:

    git clone https://github.com/sosy-lab/tbf.git tbf
    ls tbf/klee/bin
    clang  gen-random-bout  kleaver  klee  klee-replay  klee-stats  ktest-tool

Unfortunately, these depend on shared libraries, so we need to adapt both the PATH and LD_LIBRARY_PATH of the current bash session to make use of the KLEE binaries:

    export PATH=$(pwd)/tbf/klee/bin:$PATH
    export LD_LIBRARY_PATH=$(pwd)/tbf/klee/lib:$LD_LIBRARY_PATH
    klee --help #for testing if this works, should generate help output


## Installation of TBF

TBF can be cloned as git repository:

    git clone https://github.com/sosy-lab/tbf.git tbf

See the README.md for a description of basic functionality.
E.g. for checking a file using KLEE:

    ./run_iuv -i klee --execution programtocheck.c

## Installation of AFL

Get the latest tarball:

    curl -O http://lcamtuf.coredump.cx/afl/releases/afl-latest.tgz
 
 Notice that https is not available, thus the download is pure, untrusted http!?!
 Maybe use this unofficial git repo instead:
 
     git clone https://github.com/mcarpenter/afl
     cd afl
     make -j5
